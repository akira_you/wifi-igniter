#include "esp_system.h"
#define NOF_LED 100 //1250 or 160
#ifdef __cplusplus
extern "C"{
#endif
    void begin_led_task();
    void led_command(uint8_t *data,int dataLen);
#ifdef __cplusplus
}
#endif
