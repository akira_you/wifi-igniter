#include "WS2812.h"
#include <freertos/task.h>
#include <stdlib.h>
#include <string.h>
#include <esp_log.h>
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "ledTask.h"

const static char* TAG="ledTask";
class LedTask{
    xSemaphoreHandle _lock;
    void lockInit(){
        vSemaphoreCreateBinary(_lock);
    }
    void lock(){
        xSemaphoreTake(_lock,portMAX_DELAY);
    }
    void unlock(){
        xSemaphoreGive(_lock);
    }

    WS2812 *ws;   
    uint16_t pixelCount;
    uint8_t *pixBuf;
    int mode; //0 :off 1:on 2:loop
    int count;
    public:
    LedTask(){
        mode=1;
        count=0;
        ws=NULL;
        _lock=NULL;
        pixBuf=NULL;
        lockInit();
    }
    ~LedTask(){
        delete ws;
        if(pixBuf)free(pixBuf);
        pixBuf=NULL;
        ws=NULL;
    }

    void fillBlack(){
        for(int i=0;i<pixelCount;i++){
                    ws->setPixel(i,0);
        }
    }
    void fillWave_(){
        lock();
        float phase=(float)count/20.0f/5.0f;
        if(phase>1.0f){
            phase=1.0f;
            count=0;
            mode=0;
            ESP_LOGI(TAG,"fill wave end");
        }
        count++;
        unlock();
        fillBlack();
        int fireLen=10;
        const int fireSpan=11;
        const int nofFire=3;
        short color[nofFire]={0,30,60};
        short velo[nofFire]={50,100,200};
                
        for(int f=0;f<nofFire;f++){
            int hPos= (pixelCount+(nofFire-1)*fireSpan+fireLen)*phase;
            hPos-=f*fireSpan;
            int lPos= hPos-fireLen;
            if(pixelCount<hPos)hPos=pixelCount;
            if(lPos<0)lPos=0;
            if(hPos<0)continue;
            if(pixelCount<lPos)continue;
            for(int i=lPos;i<hPos;i++){
                uint8_t v= 200.0f* (i-lPos+1)/(fireLen+1);
                ws->setHSBPixel(i,color[f],255,v);
            }
        }
    }
    void fillWave(){
        lock();
        float phase=(float)count/20.0f/8.0f;
        if(phase>1.0f){
            phase=1.0f;
            count=0;
            mode=0;
            ESP_LOGI(TAG,"fill wave end");
        }
        count++;
        unlock();
        fillBlack();
        int fireLen=10;
        const int fireSpan=11;
        const int nofFire=3;
        short color[nofFire]={0,15,30};
        short velo[nofFire]={80,125,150};

        int colPhase=count%3;

        int hPos= (pixelCount+fireLen)*phase;
        int lPos= hPos-fireLen;
        if(pixelCount<hPos)hPos=pixelCount;
        if(lPos<0)lPos=0;

        for(int i=lPos;i<hPos;i++){
            uint8_t v= velo[colPhase]* (i-lPos+1)/(fireLen+1);
            ws->setHSBPixel(i,color[colPhase],255,v);
        }
    
    }
    void fillLoop(){
        const int nofFire=3;
        static short color[nofFire]={0};
        lock();
        float phase=(float)count/20.0f/8.0f;
        if(phase>1.0f){
            phase=1.0f;
            count=0;
            ESP_LOGI(TAG,"fill loop epoch");
            for(int i=0;i<nofFire;i++)color[i]=rand()%360;
        }
        count++;
        unlock();
        int fireLen=5;
        fillBlack();
        for(int i=0;i<nofFire;i++){
            
            int hPos= (pixelCount+fireLen)*phase;
            hPos+=pixelCount*i/nofFire;
            hPos%=(pixelCount+fireLen);
            int lPos= hPos-fireLen;
            if(pixelCount<hPos)hPos=pixelCount;
            if(lPos<0)lPos=0;
            for(int j=lPos;j<hPos;j++){
                uint8_t v= 200.0f* (j-lPos+1)/(fireLen+1);
                ws->setHSBPixel(j,color[i] ,255,v);
            }
        }
    }
    
    void task(void){
        const portTickType span=50/portTICK_PERIOD_MS;//20Hz
        portTickType now=xTaskGetTickCount();
        while(true){
            vTaskDelayUntil( &now, span );
            lock();
            int _mode=mode;
            unlock();
            switch(_mode){
            case 0:
                fillBlack();
                ws->show();
                break;
            case 1:
                fillWave();
                ws->show();
                break;
            case 2:
                fillLoop();
                ws->show();
            default:
                break;
            }
        }
    }

    void command(uint8_t *data,int dataLen){
        ESP_LOGI(TAG,"Command:%d",(int)data[0]);
        lock();
        int newMode=data[0];
        if(newMode!=mode){
            count=0;
            mode=newMode;
        }
        unlock();
    }
    void setup(gpio_num_t gpioNum, uint16_t _pixelCount, int channel/* RMT_CHANNEL_0*/){
        ESP_LOGI(TAG,"Init pixcel count:%d",_pixelCount);
        pixelCount=_pixelCount;
        ESP_LOGI(TAG, "Free heap1: %u  and lagest is %d", xPortGetFreeHeapSize(),heap_caps_get_largest_free_block(MALLOC_CAP_32BIT ) );
        ws=new WS2812( gpioNum,  pixelCount, channel);
        assert(ws);
        ESP_LOGI(TAG, "Free heap2: %u  and lagest is %d", xPortGetFreeHeapSize(),heap_caps_get_largest_free_block(MALLOC_CAP_32BIT ) );
        pixBuf=(uint8_t*)calloc(pixelCount*3,1);
        assert(pixBuf);
        ESP_LOGI(TAG, "Free heap3: %u  and lagest is %d", xPortGetFreeHeapSize(),heap_caps_get_largest_free_block(MALLOC_CAP_32BIT ) );

        fillBlack();
        ws->show();
        vTaskDelay(200);

        /*
        //initial is darkRed 
        for (int i=0;i<pixelCount;i++){
            uint8_t r=i%0x0f;
            uint8_t g=(i>>4)%0x0f;
            uint8_t b=(i>>8)%0x0f;
            ws->setPixel(i+1,r,g,b);
        }
        ws->show();*/
        
    }
    
};


extern "C"{
    LedTask *ledTask;
    static void _task(void * dummy){
        ledTask->task();
    }
    void begin_led_task()
    {
        ledTask=new LedTask();
        ledTask->setup(GPIO_NUM_27,NOF_LED,RMT_CHANNEL_0);
        xTaskCreatePinnedToCore(_task,"ledtask",2048,NULL,10,NULL,1);
    }
    void led_command(uint8_t *data,int dataLen){
        ledTask->command(data,dataLen);
    }

}